﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Gun_Damage : MonoBehaviour {

		private Gun_Master gunMaster;
		public int damage = 50;

		void OnEnable () {
			SetInitialReferences();
			gunMaster.EventShotEnemy += ApplyDamage;
		}

		void OnDisable () {
			gunMaster.EventShotEnemy -= ApplyDamage;
		}

		void SetInitialReferences() {
			gunMaster = GetComponent<Gun_Master>();
		}

		void ApplyDamage(Vector3 hitPosition, Transform hitTransform) {
			if (hitTransform != null) {
				if (hitTransform.GetComponent<Enemy_TakeDamage>() != null) {
					hitTransform.GetComponent<Enemy_TakeDamage>().ProcessDamage(damage);
				}
			}
		}
	}
}