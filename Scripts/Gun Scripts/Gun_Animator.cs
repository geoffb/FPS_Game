﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Gun_Animator : MonoBehaviour {

		private Gun_Master gunMaster;
		private Animator _animator;

		void OnEnable () {
			SetInitialReferences();
			gunMaster.EventPlayerInput += PlayShootAnimation;
		}

		void OnDisable () {
			gunMaster.EventPlayerInput -= PlayShootAnimation;
		}

		void SetInitialReferences() {
			gunMaster = GetComponent<Gun_Master>();

			if (GetComponent<Animator>() != null) {
				_animator = GetComponent<Animator>();
			}
		}

		void PlayShootAnimation() {
			Debug.Log("============");
			if (_animator != null) {
				_animator.SetTrigger("Shoot");
			}
		}
	}
}