﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Gun_MuzzleFlash : MonoBehaviour {

		public ParticleSystem muzzleFlash;
		private Gun_Master gunMaster;

		void OnEnable () {
			SetInitialReferences();
			gunMaster.EventPlayerInput += BurstFlash;
		}

		void OnDisable () {
			gunMaster.EventPlayerInput -= BurstFlash;
		}

		void SetInitialReferences() {
			gunMaster = GetComponent<Gun_Master>();
		}

		void BurstFlash() {
			if (muzzleFlash != null) {
				muzzleFlash.Play();
				Invoke("StopFlash", 0.10f);
			}
		}

		void StopFlash() {
			muzzleFlash.Stop();
		}
	}
}