﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Gun_Shoot : MonoBehaviour {

		private Gun_Master gunMaster;
		private Transform _transform;
		private Transform camTransform;
		private RaycastHit hit;
		public float range = 400.0f;
		public float offsetFactor = 7.0f;
		private Vector3 startPosition;

		void OnEnable () {
			SetInitialReferences();
			gunMaster.EventPlayerInput += OpenFire;
			gunMaster.EventSpeedCaptured += SetStartOfShootingPosition;
		}

		void OnDisable () {
			gunMaster.EventPlayerInput -= OpenFire;
			gunMaster.EventSpeedCaptured -= SetStartOfShootingPosition;
		}

		void SetInitialReferences() {
			gunMaster = GetComponent<Gun_Master>();
			_transform = transform;
			camTransform = _transform.parent;
		}

		void OpenFire() {
			Debug.Log("Open fire called");
			if (Physics.Raycast(camTransform.TransformPoint(startPosition), camTransform.forward, out hit, range)) {
				gunMaster.CallEventShotDefault(hit.point, hit.transform);

				if (hit.transform.CompareTag(GameManager_References._enemyTag)) {
					Debug.Log("Shot Enemy");
					gunMaster.CallEventShotEnemy(hit.point, hit.transform);
				}
			}
		}

		void SetStartOfShootingPosition(float playerSpeed) {
			float offset = playerSpeed / offsetFactor;
			startPosition = new Vector3(Random.Range(-offset, offset), Random.Range(-offset, offset), 1);
		}
	}
}