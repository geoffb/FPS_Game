﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Gun_Master : MonoBehaviour {
		public delegate void GeneralEventHandler();
		public GeneralEventHandler EventPlayerInput;
		public GeneralEventHandler EventGunNotUsable;
		public GeneralEventHandler EventRequestReload;
		public GeneralEventHandler EventRequestGunReset;
		public GeneralEventHandler EventToggleBurstFire;

		public delegate void GunHitEventHandler(Vector3 hitPosition, Transform hitTransform);
		public GunHitEventHandler EventShotDefault;
		public GunHitEventHandler EventShotEnemy;

		public delegate void GunAmmoEventHandler(int currentAmmo, int carriedAmmo);
		public GunAmmoEventHandler EventAmmoChanged;

		public delegate void GunCrosshairEventHandler(float speed);
		public GunCrosshairEventHandler EventSpeedCaptured;

		public bool isGunLoaded;
		public bool isReloading;

		public void CallEventPlayerInput() {
			if (EventPlayerInput != null) {
				EventPlayerInput();
			}
		}

		public void CallEventGunNotUsable() {
			if (EventGunNotUsable != null) {
				EventGunNotUsable();
			}
		}

		public void CallEventRequestReload() {
			if (EventRequestReload != null) {
				EventRequestReload();
			}
		}

		public void CallEventRequestGunReset() {
			if (EventRequestGunReset != null) {
				EventRequestGunReset();
			}
		}

		public void CallEventToggleBurstFire() {
			if (EventToggleBurstFire != null) {
				EventToggleBurstFire();
			}
		}

		public void CallEventShotDefault(Vector3 hitPosition, Transform hitTransform) {
			if (EventShotDefault != null) {
				EventShotDefault(hitPosition, hitTransform);
			}
		}

		public void CallEventShotEnemy(Vector3 hitPosition, Transform hitTransform) {
			if (EventShotEnemy != null) {
				EventShotEnemy(hitPosition, hitTransform);
			}
		}

		public void CallEventAmmoChanged(int currentAmmo, int carriedAmmo) {
			if (EventAmmoChanged != null) {
				EventAmmoChanged(currentAmmo, carriedAmmo);
			}
		}

		public void CallEventSpeedCaptured(float speed) {
			if (EventSpeedCaptured != null) {
				EventSpeedCaptured(speed);
			}
		}
	}
}