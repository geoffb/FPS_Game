﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Gun_HitEffects : MonoBehaviour {

		private Gun_Master gunMaster;
		public GameObject defaultHitEffect;
		public GameObject enemyHitEffect;

		void OnEnable () {
			SetInitialReferences();
			gunMaster.EventShotDefault += SpawnDefaultEffect;
			gunMaster.EventShotEnemy += SpawnEnemyEffect;
		}

		void OnDisable () {
			gunMaster.EventShotDefault -= SpawnDefaultEffect;
			gunMaster.EventShotEnemy -= SpawnEnemyEffect;
		}

		void SetInitialReferences() {
			gunMaster = GetComponent<Gun_Master>();
		}

		void SpawnDefaultEffect(Vector3 hitPosition, Transform hitTransform) {
			if (defaultHitEffect != null) {
				Instantiate(defaultHitEffect, hitPosition, Quaternion.identity);
			}
		}

		void SpawnEnemyEffect(Vector3 hitPosition, Transform hitTransform) {
			if (enemyHitEffect != null) {
				Instantiate(enemyHitEffect, hitPosition, Quaternion.identity);
			}
		}
	}
}