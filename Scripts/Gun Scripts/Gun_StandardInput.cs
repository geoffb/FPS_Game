﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Gun_StandardInput : MonoBehaviour {

		private Gun_Master gunMaster;
		private float nextAttack;
		public float attackRate = 0.5f;
		private Transform _transform;
		public bool isAutomatic;
		public bool hasBurstFire;
		private bool isBurstFireActive;
		public string attackButtonName;
		public string reloadButtonName;
		public string burstFireButtonName;

		void Start () {
			SetInitialReferences();
		}

		void Update () {
			CheckIfWeaponShouldAttack();
			CheckForReloadRequest();
			CheckForBurstFireToggle();
		}

		void SetInitialReferences() {
			gunMaster = GetComponent<Gun_Master>();
			_transform = transform;
			gunMaster.isGunLoaded = true;
		}

		void CheckIfWeaponShouldAttack() {
			if (Time.time > nextAttack && Time.timeScale > 0 && _transform.root.CompareTag(GameManager_References._playerTag)) {
				if (isAutomatic && !isBurstFireActive) {
					if (Input.GetButton(attackButtonName)) {
						Debug.Log("Full Auto");
						AttemptAttack();
					}
				}
				else if (isAutomatic && isBurstFireActive) {
					if (Input.GetButton(attackButtonName)) {
						Debug.Log("Burst Fire");
						StartCoroutine(RunBurstFire());
					}
				}
				else if (!isAutomatic) {
					if (Input.GetButton(attackButtonName)) {
						Debug.Log("Single Fire");
						AttemptAttack();
					}
				}
			}
		}

		void AttemptAttack() {
			nextAttack = Time.time + attackRate;

			if (gunMaster.isGunLoaded) {
				Debug.Log("Shooting");
				gunMaster.CallEventPlayerInput();
			}
			else {
				gunMaster.CallEventGunNotUsable();
			}
		}

		void CheckForReloadRequest() {
			if (Input.GetButtonDown(reloadButtonName) && Time.timeScale > 0 && _transform.root.CompareTag(GameManager_References._playerTag)) {
				Debug.Log("Reload");
				gunMaster.CallEventRequestReload();
			}
		}

		void CheckForBurstFireToggle() {
			if (Input.GetButtonDown(burstFireButtonName) && Time.timeScale > 0 && _transform.root.CompareTag(GameManager_References._playerTag)) {
				isBurstFireActive = !isBurstFireActive;
				gunMaster.CallEventToggleBurstFire();
				Debug.Log("Burst Fire Toggled");
			}
		}

		IEnumerator RunBurstFire() {
			// Burst is 3 fire attack
			AttemptAttack();
			yield return new WaitForSeconds(attackRate);
			AttemptAttack();
			yield return new WaitForSeconds(attackRate);
			AttemptAttack();
		}
	}
}