﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class TestGameOver : MonoBehaviour {
		void Update () {
			if(Input.GetKeyDown(KeyCode.O)) {
				GetComponent<GameManager_Master>().CallGameOverEvent();
			}
		}
	}
}