﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class GameManager_ToggleInventory : MonoBehaviour {

		private GameManager_Master gameManager;
		public bool hasInventory = true;
		public GameObject inventoryUI;
		public string toggleInventoryButton;

		void Start () {
			SetInitialReferences();
		}

		void Update () {
			CheckForInventoryUIToggleRequest();
		}

		void SetInitialReferences() {
			gameManager = GetComponent<GameManager_Master>();

			if (toggleInventoryButton == "") {
				Debug.LogWarning("Please enter a name for the inventory button");
				this.enabled = false;
			}
		}

		void CheckForInventoryUIToggleRequest() {
			if (Input.GetButtonDown(toggleInventoryButton) && !gameManager.isMenuOn && !gameManager.isGameOver && hasInventory) {
				ToggleInventoryUI();
			}
		}

		public void ToggleInventoryUI() {
			if (inventoryUI != null) {
				inventoryUI.SetActive(!inventoryUI.activeSelf);
				gameManager.isIventoryUIOn = !gameManager.isIventoryUIOn;
				gameManager.CallInventoryUIToggleEvent();
			}
		}
	}
}