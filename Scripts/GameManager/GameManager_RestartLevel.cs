﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GeoffEngine {
	public class GameManager_RestartLevel : MonoBehaviour {
		private GameManager_Master gameManager;

		void OnEnable() {
			SetInitialReferences();
			gameManager.RestartLevel += RestartLevel;
		}

		void OnDisable() {
			gameManager.RestartLevel -= RestartLevel;
		}

		void SetInitialReferences() {
			gameManager = GetComponent<GameManager_Master>();
		}

		void RestartLevel() {
			int scene = SceneManager.GetActiveScene().buildIndex;
			SceneManager.LoadScene(scene);
		}
	}
}