﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GeoffEngine {
	public class MainMenu : MonoBehaviour {

		public void PlayGame() {
			SceneManager.LoadScene("Level1");
		}

		public void ExitGame() {
			Application.Quit();
		}
	}
}