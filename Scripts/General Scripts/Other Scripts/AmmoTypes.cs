﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	[System.Serializable]
	public class AmmoTypes {
		public string ammoName;
		public int ammoCurrentCarried;
		public int ammoMaxQuantity;

		public AmmoTypes(string _ammoName, int _numberAmmo, int _maxQuantity) {
			this.ammoName = _ammoName;
			this.ammoCurrentCarried = _numberAmmo;
			this.ammoMaxQuantity = _maxQuantity;
		}
	}
}