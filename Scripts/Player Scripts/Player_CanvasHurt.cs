﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Player_CanvasHurt : MonoBehaviour {
		public GameObject hurtCanvas;
		private Player_Master playerManager;
		private float secondsUntilHidden = 0.7f;

		void OnEnable () {
			SetInitialReferences();
			playerManager.EventPlayerHealthDecrease += TurnOnHurtEffect;
		}

		void OnDisable () {
			playerManager.EventPlayerHealthDecrease -= TurnOnHurtEffect;
		}

		void SetInitialReferences() {
			playerManager = GetComponent<Player_Master>();	
		}

		void TurnOnHurtEffect(int neverUsed) {
			if (hurtCanvas != null) {
				hurtCanvas.SetActive(true);
				StartCoroutine(ResetHurtCanvas());
			}
		}

		IEnumerator ResetHurtCanvas() {
			yield return new WaitForSeconds(secondsUntilHidden);
			hurtCanvas.SetActive(false);
		}
	}
}