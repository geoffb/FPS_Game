﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GeoffEngine {
	public class Player_Health : MonoBehaviour {

		private GameManager_Master gameManager;
		private Player_Master playerManager;
		public int playerHealth;
		public int maxHealth = 100;
		public Text healthText;

		void OnEnable () {
			SetInitialReferences();
			SetUI();
			playerManager.EventPlayerHealthDecrease += DecreaseHealth;
			playerManager.EventPlayerHealthIncrease += IncreaseHealth;
		}

		void OnDisable () {
			playerManager.EventPlayerHealthDecrease -= DecreaseHealth;
			playerManager.EventPlayerHealthIncrease -= IncreaseHealth;
		}

		void Start () {
			//StartCoroutine(TestHealthDecrease());
		}

		void SetInitialReferences() {
			gameManager = GameObject.Find("GameManager").GetComponent<GameManager_Master>();
			playerManager = GetComponent<Player_Master>();
		}

		IEnumerator TestHealthDecrease() {
			yield return new WaitForSeconds(4);
			//DecreaseHealth(100);
			playerManager.CallEventPlayerHealthDecrease(50);
		}

		void DecreaseHealth(int healthChange) {
			playerHealth -= healthChange;

			if (playerHealth <= 0) {
				playerHealth = 0; // For UI no negative numbers
				gameManager.CallGameOverEvent();
			}

			SetUI();
		}

		void IncreaseHealth(int healthChange) {
			playerHealth += healthChange;

			if (playerHealth > maxHealth) {
				playerHealth = maxHealth;
			}

			SetUI();
		}

		void SetUI() {
			if (healthText != null) {
				healthText.text = playerHealth.ToString();
			}
		}
	}
}