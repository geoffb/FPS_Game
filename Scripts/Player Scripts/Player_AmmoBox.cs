﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Player_AmmoBox : MonoBehaviour {
		
		private Player_Master playerManager;

		public List<AmmoTypes> ammoTypes = new List<AmmoTypes>();

		void OnEnable () {
			SetInitialReferences();
			playerManager.EventPickedUpAmmo += PickedUpAmmo;
		}

		void OnDisable () {
			playerManager.EventPickedUpAmmo -= PickedUpAmmo;
		}

		void SetInitialReferences() {
			playerManager = GetComponent<Player_Master>();
		}

		void PickedUpAmmo(string _ammoName, int _quantity) {
			for (int i = 0; i < ammoTypes.Count;i++) {
				if (ammoTypes[i].ammoName == _ammoName) {
					ammoTypes[i].ammoCurrentCarried += _quantity;

					if (ammoTypes[i].ammoCurrentCarried > ammoTypes[i].ammoMaxQuantity) {
						ammoTypes[i].ammoCurrentCarried = ammoTypes[i].ammoMaxQuantity;
					}

					playerManager.CallEventAmmoChanged();

					break;
				}
				else {
					// Create entry
				}
			}
		}
	}
}