﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GeoffEngine {
	public class Player_Inventory : MonoBehaviour {

		public Transform inventoryPlayerParent;
		public Transform inventoryUIParent;
		public GameObject UIButton;

		private Player_Master playerManager;
		private GameManager_ToggleInventory inventoryUIScript;
		private float timeToPlaceInHands = 0.1f;
		private Transform currentHeldItem;
		private int counter;
		private string buttonText;
		private List<Transform> listInventory = new List<Transform>();

		void OnEnable () {
			SetInitialReferences();
			DeactivateAllInventoryItems();
			UpdateInventoryListAndUI();
			CheckIfHandsEmpty();

			playerManager.EventIventoryChanged += UpdateInventoryListAndUI;
			playerManager.EventIventoryChanged += CheckIfHandsEmpty;
			playerManager.EventHandsEmpty += ClearHands;
		}

		void OnDisable () {
			playerManager.EventIventoryChanged -= UpdateInventoryListAndUI;
			playerManager.EventIventoryChanged -= CheckIfHandsEmpty;
			playerManager.EventHandsEmpty -= ClearHands;
		}

		void SetInitialReferences() {
			playerManager = GetComponent<Player_Master>();
			inventoryUIScript = GameObject.Find("GameManager").GetComponent<GameManager_ToggleInventory>();
		}

		void UpdateInventoryListAndUI() {
			counter = 0;
			listInventory.Clear();
			listInventory.TrimExcess();

			ClearInventoryUI();

			foreach (Transform child in inventoryPlayerParent) {
				if (child.CompareTag("Item")) {
					listInventory.Add(child);
					GameObject go = Instantiate(UIButton) as GameObject;
					buttonText = child.name;
					go.GetComponentInChildren<Text>().text = buttonText;
					int index = counter;
					// Need delegate here to pass the parameter to the function
					go.GetComponent<Button>().onClick.AddListener(delegate {ActivateInventoryItem(index);});
					go.GetComponent<Button>().onClick.AddListener(inventoryUIScript.ToggleInventoryUI);
					go.transform.SetParent(inventoryUIParent, false);
					counter++;
				}
			}
		}

		void CheckIfHandsEmpty() {
			if (currentHeldItem == null && listInventory.Count > 0) {
				// Get the last weapon
				StartCoroutine(PlaceItemInHands(listInventory[listInventory.Count - 1]));
			}
		}

		void ClearHands() {
			currentHeldItem = null;
		}

		void ClearInventoryUI() {
			foreach (Transform child in inventoryUIParent) {
				Destroy(child.gameObject);
			}
		}

		public void ActivateInventoryItem(int inventoryIndex) {
			DeactivateAllInventoryItems();
			StartCoroutine(PlaceItemInHands(listInventory[inventoryIndex]));
		}

		void DeactivateAllInventoryItems() {
			foreach (Transform child in inventoryPlayerParent) {
				if (child.CompareTag("Item")) {
					child.gameObject.SetActive(false);
				}
			}
		}

		IEnumerator PlaceItemInHands(Transform itemTransform) {
			yield return new WaitForSeconds(timeToPlaceInHands);
			currentHeldItem = itemTransform;
			currentHeldItem.gameObject.SetActive(true);
		}
	}
}