﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Player_Master : MonoBehaviour {
		public delegate void GeneralEventHandler();
		public event GeneralEventHandler EventIventoryChanged;
		public event GeneralEventHandler EventHandsEmpty;
		public event GeneralEventHandler EventAmmoChanged;

		public delegate void AmmoPickupEventHandler(string ammoName, int ammoQuantity);
		public event AmmoPickupEventHandler EventPickedUpAmmo;

		public delegate void PlayerHealthEventHandler(int healthChange);
		public event PlayerHealthEventHandler EventPlayerHealthDecrease;
		public event PlayerHealthEventHandler EventPlayerHealthIncrease;

		public void CallEventIventoryChanged() {
			if (EventIventoryChanged != null) {
				EventIventoryChanged();
			}
		}

		public void CallEventHandsEmpty() {
			if (EventHandsEmpty != null) {
				EventHandsEmpty();
			}
		}

		public void CallEventAmmoChanged() {
			if (EventAmmoChanged != null) {
				EventAmmoChanged();
			}
		}

		public void CallEventPickedUpAmmo(string ammoName, int ammoQuantity) {
			if (EventPickedUpAmmo != null) {
				EventPickedUpAmmo(ammoName, ammoQuantity);
			}
		}

		public void CallEventPlayerHealthDecrease(int healthChange) {
			if (EventPlayerHealthDecrease != null) {
				EventPlayerHealthDecrease(healthChange);
			}
		}

		public void CallEventPlayerHealthIncrease(int healthChange) {
			if (EventPlayerHealthIncrease != null) {
				EventPlayerHealthIncrease(healthChange);
			}
		}
	}
}