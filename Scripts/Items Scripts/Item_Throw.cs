﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Item_Throw : MonoBehaviour {

		private Item_Master itemMaster;
		private Transform _Transform;
		private Rigidbody _Rigidbody;
		private Vector3 throwDirection;

		public bool canBeThrown;
		public string throwButtonName;
		public float throwForce;

		void OnEnable () {
			SetInitialReferences();
			
		}

		void Update () {
			CheckForThrowInput();
		}

		void SetInitialReferences() {
			itemMaster = GetComponent<Item_Master>();
			_Transform = transform;
			_Rigidbody = GetComponent<Rigidbody>();
		}

		void CheckForThrowInput() {
			if (Input.GetButtonDown(throwButtonName) && Time.time > 0 && canBeThrown && _Transform.root.CompareTag(GameManager_References._playerTag)) {
				CarryOutThrowActions();
			}
		}

		void CarryOutThrowActions() {
			throwDirection = _Transform.parent.forward;
			_Transform.parent = null; // Separate item from player
			itemMaster.CallEventObjectThrow();
			HurlItem(); 
		}

		void HurlItem() {
			_Rigidbody.AddForce(throwDirection * throwForce, ForceMode.Impulse);
		}
	}
}