﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Item_Collider : MonoBehaviour {

		private Item_Master itemMaster;
		public Collider[] _colliders;
		public PhysicMaterial _physicMaterial;

		void OnEnable () {
			SetInitialReferences();
			itemMaster.EventObjectThrow += EnableColliders;
			itemMaster.EventObjectPickup += DisableColliders;
		}

		void OnDisable () {
			itemMaster.EventObjectThrow -= EnableColliders;
			itemMaster.EventObjectPickup -= DisableColliders;
		}

		void Start () {
			CheckIfStartsInInventory();
		}

		void Update () {
			
		}

		void SetInitialReferences() {
			itemMaster = GetComponent<Item_Master>();
			_colliders = GetComponentsInChildren<Collider>();
		}

		void CheckIfStartsInInventory() {
			if (transform.root.CompareTag(GameManager_References._playerTag)) {
				DisableColliders();
			}
		}

		void EnableColliders() {
			if (_colliders.Length > 0) {
				foreach (Collider col in _colliders) {
					col.enabled = true;

					if (_physicMaterial != null) {
						col.material = _physicMaterial;
					}
				}
			}
		}

		void DisableColliders() {
			if (_colliders.Length > 0) {
				foreach (Collider col in _colliders) {
					col.enabled = false;
				}
			}
		}
	}
}