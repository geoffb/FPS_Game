﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Item_Name : MonoBehaviour {

		public string itemName;

		void Start () {
			SetItemName();
		}

		void SetItemName() {
			if (itemName != "") {
				transform.name = itemName;
			}
		}
	}
}