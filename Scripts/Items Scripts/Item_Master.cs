﻿   using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Item_Master : MonoBehaviour {

		private Player_Master playerManager;

		public delegate void GeneralEventHandler();
		public event GeneralEventHandler EventObjectThrow;
		public event GeneralEventHandler EventObjectPickup;

		public delegate void PickupActionEventHandler(Transform item);
		public event PickupActionEventHandler EventPickupAction;

		void Start() {
			SetInitialReferences(); // Script exec order. Instead of OnEnable
		}

		public void CallEventObjectThrow() {
			if (EventObjectThrow != null) {
				EventObjectThrow();
			}

			playerManager.CallEventHandsEmpty();
			playerManager.CallEventIventoryChanged();
		}

		public void CallEventObjectPickup() {
			if (EventObjectPickup != null) {
				EventObjectPickup();
			}

			playerManager.CallEventIventoryChanged();
		}

		public void CallEventPickupAction(Transform item) {
			if (EventPickupAction != null) {
				EventPickupAction(item);
			}
		}

		void SetInitialReferences() {
			if (GameManager_References._player != null) {
				playerManager = GameManager_References._player.GetComponent<Player_Master>();
			}
		}
	}
}