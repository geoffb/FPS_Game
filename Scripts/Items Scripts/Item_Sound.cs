﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Item_Sound : MonoBehaviour {

		private Item_Master itemMaster;
		public float defaultVolume; 
		public AudioClip itemSound;

		void OnEnable () {
			SetInitialReferences();
			itemMaster.EventObjectThrow += PlayThrowSound;
		}

		void OnDisable () {
			itemMaster.EventObjectThrow -= PlayThrowSound;
		}

		void SetInitialReferences() {
			itemMaster = GetComponent<Item_Master>();
		}

		void PlayThrowSound() {
			AudioSource.PlayClipAtPoint(itemSound, transform.position, defaultVolume);
		}
	}
}