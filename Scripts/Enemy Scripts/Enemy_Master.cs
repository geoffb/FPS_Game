﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Enemy_Master : MonoBehaviour {
		
		public Transform myTarget;
		public bool isOnRoute;
		public bool isNavPaused;

		public delegate void GeneralEventHandler();
		public event GeneralEventHandler EventEnemyDie;
		public event GeneralEventHandler EventEnemyAttack;
		public event GeneralEventHandler EventEnemyWalking;
		public event GeneralEventHandler EventEnemyLostTarget;
		public event GeneralEventHandler EventEnemyReachedNavTarget;

		public delegate void HealthEventHandler(int healthChange);
		public event HealthEventHandler EventEnemyDeductHealth;

		public delegate void NavTargetEventHandler(Transform targetTransform);
		public event NavTargetEventHandler EventEnemySetNavTarget;

		public void CallEventEnemyDie() {
			if (EventEnemyDie != null) {
				EventEnemyDie();
			}
		}

		public void CallEventEnemyAttack() {
			if (EventEnemyAttack != null) {
				EventEnemyAttack();
			}
		}

		public void CallEventEnemyWalking() {
			if (EventEnemyWalking != null) {
				EventEnemyWalking();
			}
		}

		public void CallEventEnemyLostTarget() {
			if (EventEnemyLostTarget != null) {
				EventEnemyLostTarget();
			}

			myTarget = null;
		}

		public void CallEventEnemyReachedNavTarget() {
			if (EventEnemyReachedNavTarget != null) {
				EventEnemyReachedNavTarget();
			}
		}

		public void CallEventEnemyDeductHealth(int healthChange) {
			if (EventEnemyDeductHealth != null) {
				EventEnemyDeductHealth(healthChange);
			}
		}

		public void CallEventEnemySetNavTarget(Transform targetTransform) {
			if (EventEnemySetNavTarget != null) {
				EventEnemySetNavTarget(targetTransform);
			}

			myTarget = targetTransform;
		}
	}
}