﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace GeoffEngine {
	public class Enemy_NavPersue : MonoBehaviour {

		private Enemy_Master enemyMaster;
		private NavMeshAgent _navMeshAgent;
		private float checkRate;
		private float nextCheck;

		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableThis;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDie -= DisableThis;
		}

		void Update () {
			if (Time.time > nextCheck) {
				nextCheck = Time.time + checkRate;
				TryToChaseTarget();
			}
		}

		void SetInitialReferences() {
			enemyMaster = GetComponent<Enemy_Master>();

			if (GetComponent<NavMeshAgent>() != null) {
				_navMeshAgent = GetComponent<NavMeshAgent>();
			}

			checkRate = Random.Range(0.1f, 0.2f);
		}

		void TryToChaseTarget() {
			if (enemyMaster.myTarget != null && _navMeshAgent != null && !enemyMaster.isNavPaused) {
				_navMeshAgent.SetDestination(enemyMaster.myTarget.position);

				if (_navMeshAgent.remainingDistance > _navMeshAgent.stoppingDistance) {
					enemyMaster.CallEventEnemyWalking();
					enemyMaster.isOnRoute = true;
				}
			}
		}

		void DisableThis() {
			if (_navMeshAgent != null) {
				_navMeshAgent.enabled = false;
			}

			this.enabled = false;
		}
	}
}