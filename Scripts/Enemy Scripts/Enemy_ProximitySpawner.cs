﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Enemy_ProximitySpawner : MonoBehaviour {

		public GameObject objectToSpawn;
		public int numberToSpawn;
		public float proximity;
		private float checkRate;
		private float nextCheck;
		private Transform _transform;
		private Transform playerTransform;
		private Vector3 spawnPosition;

		void Start () {
			SetInitialReferences();
		}

		void Update () {
			if (Time.time > nextCheck) {
				nextCheck = Time.time + checkRate;
				CheckDistance();
			}
		}

		void SetInitialReferences() {
			_transform = transform;
			playerTransform = GameManager_References._player.transform;
			checkRate = Random.Range(0.8f, 1.2f);
		}

		void CheckDistance() {
			if (Vector3.Distance(_transform.position, playerTransform.position) < proximity) {
				SpawnObject();
				this.enabled = false;
			}
		}

		void SpawnObject() {
			for(int i = 0; i < numberToSpawn; i++) {
				spawnPosition = _transform.position + Random.insideUnitSphere * 5;
				Instantiate(objectToSpawn, spawnPosition, _transform.rotation);
			}
		}
	}
}