﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Enemy_RagdollActivation : MonoBehaviour {

		private Enemy_Master enemyMaster;
		private Collider _collider;
		private Rigidbody _rigidbody;

		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDie += ActivateRagdoll;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDie -= ActivateRagdoll;
		}

		void ActivateRagdoll() {
			if (_rigidbody != null) {
				_rigidbody.isKinematic = false;
				_rigidbody.useGravity = true;
			}

			if (_collider != null) {
				_collider.isTrigger = false;
				_collider.enabled = true;
			}
		}

		void SetInitialReferences() {
			enemyMaster = transform.root.GetComponent<Enemy_Master>();

			if (GetComponent<Collider>() != null) {
				_collider = GetComponent<Collider>();
			}

			if (GetComponent<Rigidbody>() != null) {
				_rigidbody = GetComponent<Rigidbody>();
			}
		}
	}
}