﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Enemy_CollisionField : MonoBehaviour {

		private Enemy_Master enemyMaster;
		private Rigidbody rigidBodyStrickingMe;
		private int damageToApply;
		public float massRequirement = 50;
		public float speedRequirement = 5;
		private float damageFactor = 0.1f;

		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableThis;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDie -= DisableThis;
		}

		void OnTriggerEnter(Collider other) {
			if (other.GetComponent<Rigidbody>() != null) {
				rigidBodyStrickingMe = other.GetComponent<Rigidbody>();

				if (rigidBodyStrickingMe.mass >= massRequirement && rigidBodyStrickingMe.velocity.sqrMagnitude > speedRequirement) {
					damageToApply = (int)(damageFactor * rigidBodyStrickingMe.mass * rigidBodyStrickingMe.velocity.magnitude);
					//Debug.Log(damageToApply);
					enemyMaster.CallEventEnemyDeductHealth(damageToApply);
				}
			}
		}

		void SetInitialReferences() {
			enemyMaster = transform.root.GetComponent<Enemy_Master>();
		}

		void DisableThis() {
			gameObject.SetActive(false);
		}
	}
}