﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace GeoffEngine {
	public class Enemy_NavWander : MonoBehaviour {
		private Enemy_Master enemyMaster;
		private NavMeshAgent _navMeshAgent;
		private float checkRate;
		private float nextCheck;
		private float wanderRange = 12.0f;
		private Transform _transform;
		private NavMeshHit navHit;
		private Vector3 wanderTarget;

		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableThis;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDie -= DisableThis;
		}

		void Update () {
			if (Time.time > nextCheck) {
				nextCheck = Time.time + checkRate;
				CheckIfIShouldWander();
			}
		}

		void SetInitialReferences() {
			enemyMaster = GetComponent<Enemy_Master>();

			if (GetComponent<NavMeshAgent>() != null) {
				_navMeshAgent = GetComponent<NavMeshAgent>();
			}

			checkRate = Random.Range(0.4f, 1.0f);
			_transform = transform;
		}

		void DisableThis() {
			this.enabled = false;
		}

		void CheckIfIShouldWander() {
			if (enemyMaster.myTarget == null && !enemyMaster.isOnRoute && !enemyMaster.isNavPaused) {
				if (RandomWanderTarget(_transform.position, wanderRange, out wanderTarget)) {
					enemyMaster.isOnRoute = true;
					enemyMaster.CallEventEnemyWalking();
					_navMeshAgent.SetDestination(wanderTarget);
				}
			}
		}

		bool RandomWanderTarget(Vector3 centre, float range, out Vector3 result) {
			Vector3 randomPoint = centre + Random.insideUnitSphere * wanderRange;

			if (NavMesh.SamplePosition(randomPoint, out navHit, 1.0f, NavMesh.AllAreas)) {
				result = navHit.position;
				return true;
			}
			else {
				result = centre;
				return false;
			}
		}
	}
}