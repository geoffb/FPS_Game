﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Enemy_Detection : MonoBehaviour {

		private Enemy_Master enemyMaster;
		private Transform _transform;
		public Transform head;
		public LayerMask playerLayer;
		public LayerMask sightLayer;
		private float checkRate;
		private float nextCheck;
		private float detectRadius = 20.0f;
		private RaycastHit hit;

		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableThis;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDie -= DisableThis;
		}

		void Update () {
			CarrayOutDetection();
		}

		void SetInitialReferences() {
			enemyMaster = GetComponent<Enemy_Master>();
			_transform = transform;

			if (head == null) {
				head = _transform;
			}

			// Overlap sphere take a lot of ressources, so it's better to not have the same check time for all enemies
			checkRate = Random.Range(0.8f, 1.2f);
		}

		void CarrayOutDetection() {
			if (Time.time > nextCheck) {
				nextCheck = Time.time + checkRate;
				Collider[] colliders = Physics.OverlapSphere(_transform.position, detectRadius, playerLayer);

				if (colliders.Length > 0) {
					foreach (Collider potentialTragetCollider in colliders) {
						if (potentialTragetCollider.CompareTag(GameManager_References._playerTag)) {

							if (CanPotentialTargetBeSeen(potentialTragetCollider.transform)) {
								break;
							}
						}
					}
				}
				else {
					enemyMaster.CallEventEnemyLostTarget();
				}

			}
		}

		bool CanPotentialTargetBeSeen(Transform potentialTarget) {
			if (Physics.Linecast(head.position, potentialTarget.position, out hit, sightLayer)) {
				if (hit.transform == potentialTarget) {
					enemyMaster.CallEventEnemySetNavTarget(potentialTarget);
					return true;
				}
			}

			enemyMaster.CallEventEnemyLostTarget();
			return false;
		}

		void DisableThis() {
			this.enabled = false;
		}
	}
}