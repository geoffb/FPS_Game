﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Enemy_Attack : MonoBehaviour {
		
		private Enemy_Master enemyMaster;
		public Transform attackTarget;
		private Transform _transform;
		private float attackRate = 2.0f;
		private float nextAttack;
		public float attackRange = 3.5f;
		public int attackDamage = 10;


		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableThis;
			enemyMaster.EventEnemySetNavTarget += SetAttackTarget;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDie -= DisableThis;
			enemyMaster.EventEnemySetNavTarget -= SetAttackTarget;
		}

		void Update () {
			TryToAttack();
		}

		void SetInitialReferences() {
			enemyMaster = GetComponent<Enemy_Master>();
			_transform = transform;
		}

		void SetAttackTarget(Transform target) {
			attackTarget = target;
		}

		void TryToAttack() {
			if (attackTarget != null && Time.time > nextAttack) {
				nextAttack = Time.time + attackRate;

				if (Vector3.Distance(_transform.position, attackTarget.position) <= attackRange) {
					Vector3 lookAtVector = new Vector3(attackTarget.position.x, _transform.position.y, attackTarget.position.z);
					_transform.LookAt(lookAtVector);
					enemyMaster.CallEventEnemyAttack();
					enemyMaster.isOnRoute = false;
				}
			}
		}

		// Called by Punch Animator
		public void OnEnemyAttack() {
			if (attackTarget != null) {
				if (Vector3.Distance(_transform.position, attackTarget.position) <= attackRange) {
					if (attackTarget.GetComponent<Player_Master>() != null) {
						Vector3 toOther = attackTarget.position - _transform.position;
						//Debug.Log(Vector3.Dot(toOther, _transform.forward).ToString());

						if (Vector3.Dot(toOther, _transform.forward) > 0.5f) {
							attackTarget.GetComponent<Player_Master>().CallEventPlayerHealthDecrease(attackDamage);	
						}
					}
				}
			}
		}

		void DisableThis() {
			this.enabled = false;
		}
	}
}