﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Enemy_Animation : MonoBehaviour {

		private Enemy_Master enemyMaster;
		private Animator _animator;

		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableAnimator;
			enemyMaster.EventEnemyAttack += SetAnimationToAttack;
			enemyMaster.EventEnemyDeductHealth += SetAnimationToStruck;
			enemyMaster.EventEnemyReachedNavTarget += SetAnimationToIdle;
			enemyMaster.EventEnemyWalking += SetAnimationToWalk;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDie -= DisableAnimator;
			enemyMaster.EventEnemyAttack -= SetAnimationToAttack;
			enemyMaster.EventEnemyDeductHealth -= SetAnimationToStruck;
			enemyMaster.EventEnemyReachedNavTarget -= SetAnimationToIdle;
			enemyMaster.EventEnemyWalking -= SetAnimationToWalk;
		}

		void SetInitialReferences() {
			enemyMaster = GetComponent<Enemy_Master>();

			if (GetComponent<Animator>() != null) {
				_animator = GetComponent<Animator>();
			}
		}

		void SetAnimationToWalk() {
			if (_animator != null && _animator.enabled) {
				_animator.SetBool("isPursuing", true);
			}
		}

		void SetAnimationToIdle() {
			if (_animator != null && _animator.enabled) {
				_animator.SetBool("isPursuing", false);
			}
		}

		void SetAnimationToAttack() {
			if (_animator != null && _animator.enabled) {
				_animator.SetTrigger("Attack");
			}
		}

		void SetAnimationToStruck(int dummy) {
			if (_animator != null && _animator.enabled) {
				_animator.SetTrigger("Struck");
			}
		}

		void DisableAnimator() {
			if (_animator != null && _animator.enabled) {
				_animator.enabled = false;
			}
		}
	}
}