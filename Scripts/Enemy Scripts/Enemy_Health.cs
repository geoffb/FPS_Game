﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoffEngine {
	public class Enemy_Health : MonoBehaviour {

		private Enemy_Master enemyMaster;
		public int enemyHealth = 100;


		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDeductHealth += deductHealth;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDeductHealth -= deductHealth;
		}

		void SetInitialReferences() {
			enemyMaster = GetComponent<Enemy_Master>();
		}

		void deductHealth(int healthChange) {
			enemyHealth -= healthChange;

			if (enemyHealth < 0) {
				enemyHealth = 0;
				enemyMaster.CallEventEnemyDie();
				Destroy(gameObject, 10.0f);
			}
		}
	}
}