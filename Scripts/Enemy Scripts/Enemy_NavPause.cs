﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace GeoffEngine {
	public class Enemy_NavPause : MonoBehaviour {

		private Enemy_Master enemyMaster;
		private NavMeshAgent _navMeshAgent;
		private float pauseTime = 2.0f;

		void OnEnable () {
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableThis;
			enemyMaster.EventEnemyDeductHealth += PauseNavMeshAgent;
		}

		void OnDisable () {
			enemyMaster.EventEnemyDie -= DisableThis;
			enemyMaster.EventEnemyDeductHealth -= PauseNavMeshAgent;
		}

		void SetInitialReferences() {
			enemyMaster = GetComponent<Enemy_Master>();

			if (GetComponent<NavMeshAgent>() != null) {
				_navMeshAgent = GetComponent<NavMeshAgent>();
			}
		}

		void PauseNavMeshAgent(int dummy) {
			if (_navMeshAgent != null && _navMeshAgent.enabled) {
				_navMeshAgent.ResetPath();
				enemyMaster.isNavPaused = true;
				StartCoroutine(RestartNavMeshAgent());
			}
		}

		IEnumerator RestartNavMeshAgent() {
			yield return new WaitForSeconds(pauseTime);
			enemyMaster.isNavPaused = false;
		}

		void DisableThis() {
			StopAllCoroutines();
		}
	}
}